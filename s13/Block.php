<?php

final class Block
{
public $address = "Nové Homole";
}

// Není možné utvořit extends, když final

class Appartment extends Block{

}

$myAppartment = new Appartment();
echo $myAppartment->address;