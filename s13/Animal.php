<?php

final class Animal
{
    public function makeSound()
    {
       echo "Vrrrr";
}
}

// Zase nelze
class Dog extends Animal{

    public function makeSound()
    {
        echo "Haf, haf";
    }
}

$dog = new Dog();
$dog->makeSound();