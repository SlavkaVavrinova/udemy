<?php

class Cleaner
{
    public $name;

    public function sweepTheFloor()
    {
        echo "Prosím zameť podlahu";
        $this->name= "Pepíku";
        return $this;
}

    public function mopTheFloor()
    {
        echo " a vytři ji ". $this->name.".";
}

}

$cleaner1 = new Cleaner();
$cleaner1->sweepTheFloor()->mopTheFloor();