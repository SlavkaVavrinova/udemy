<?php

class Factory
{
    public function process1()
    {
        echo "Proces 1 vykonán.";
        return $this;
    }
    public function process2()
    {
        echo "Proces 2 vykonán.";
        return $this;
    }
    public function process3()
    {
        echo "Proces 3 vykonán.";
    }
}

$factory1 = new Factory();
$factory1->process1()->process2()->process3();