<?php

class Greeting
{
    public function hello()
    {
        echo "Ahoj sousede! ";
        return $this;
    }

    public function goodbye()
    {
        echo "Já jdu. Měj se.";
    }
}

$greeting1 = new Greeting();
$greeting1->hello()->goodbye();