<?php

class Bank
{
public $balance = 80;
public $amount;

    public function account()
    {
  echo "Tvůj účet má stav ".$this->balance." eur. ";
  $this->amount=100;
  return $this;

}
    public function withdraw()
    {
        echo "Nemůžeš vybrat ".$this->amount." eur. ";
        $this->balance=75;
return $this;
    }

    public function tryAgain()
    {
        echo "Můžeš vybrat méně než ".$this->balance." eur. ";

    }
}

$account1 = new Bank;
$account1->account()->withdraw()->tryAgain();