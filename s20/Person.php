<?php


class Person
{
private $name;
private $age;

    public function setInfo(string $name, int $age):void
    {
        $this->name=$name;
        $this->age=$age;
}

    public function getInfo():string
    {
        return "Jmenuji se $this->name a je mi $this->age";
}
}

$person1 = new Person();
$person1->setInfo("Slávka", 36);
echo $person1->getInfo();
