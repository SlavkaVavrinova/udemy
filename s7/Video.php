<?php

class Video
{
public $type = "";
public $duration = "";

public function setVideoInfo($video_type, $video_duration){
    $this->type=$video_type;
    $this->duration = $video_duration;
}

public function getVideoInfo(){
    return "Toto je ".$this->type." formát videa a je dlouhý mimimálně ". $this->duration." sekund.";
}
}

$myVideo = new Video();
$myVideo->setVideoInfo(".mp4", 45);
echo $myVideo->getVideoInfo();