<?php

class Teacher
{
    public $name = "";

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        echo "Můj učitel se jmenuje " . $this->name . ".";
    }

}

$myTeacher = new Teacher();
$myTeacher->setName("Novák");
$myTeacher->getName();