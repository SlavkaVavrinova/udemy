<?php

class Bicycle
{
public $type = "horské kolo";
public $color = "modrá";

public function giveBicycleDetails(){
    echo "Toto $this->type máme skladem v barvě: $this->color";
}
}

$bicycle1 = new Bicycle();
$bicycle1->giveBicycleDetails();