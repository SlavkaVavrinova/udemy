<?php

class FullName
{
    static public $fullName;

    public static function sayMyName($myFirstName, $myLastName){
        self::$fullName="Mé celé jméno je " . $myFirstName . " " . $myLastName;
    }

}

FullName::sayMyName("Slávka", "Vavřinová");
echo FullName::$fullName;