<?php

class Animal
{
public $name = "Alík";

protected function getName(){
    return "Ten pes se jmenuje " . $this->name;

}
}

class Animal2 extends Animal
{
      public function getDogName(){
        echo $this->getName();

    }
}

$dog1 = new Animal2();
$dog1->getDogName();

