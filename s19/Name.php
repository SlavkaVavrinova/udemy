<?php

abstract class Name
{
abstract protected function sayMyName($name);
}

class MyName extends Name{
    public function sayMyName($name)
    {
        return "Jmenuje se $name";
    }
}

$myName = new MyName();
echo $myName->sayMyName("Slávka");