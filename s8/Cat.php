<?php

class cat
{
public $color;
public $weight;

    /**
     * @param $color
     * @param $weight
     */
    public function __construct($color, $weight)
    {
        $this->color = $color;
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function describeCat()
    {
        echo "Tato $this->color kočka váží $this->weight kg.";
    }
}

$tlustoska = new cat("černá", 10);
$tlustoska->describeCat();