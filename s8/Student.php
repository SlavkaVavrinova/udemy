<?php

class Student
{
public $name;
public $grade;

      public function __construct($name, $grade)
    {
        $this->name = $name;
        $this->grade = $grade;
    }

       public function printStudentInfo()
    {
        echo "$this->name má vzdělání: $this->grade.";
    }

}

$slavka = new Student("Slávka", "VŠ");
$slavka->printStudentInfo();