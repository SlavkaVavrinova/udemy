<?php

class Message
{

    public function __construct()
    {
        echo "Toto je nová zpráva <br/>";
    }

    public function __destruct()
    {
        echo "Tato zpráva byla smazaná.";
    }
}

$sms = new Message;