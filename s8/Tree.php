<?php

class Tree
{
    public $leavesColor;
    public $numberOfLeaves;

    /**
     * @param $leavesColor
     * @param $numberOfLeaves
     */
    public function __construct($leavesColor ="", $numberOfLeaves="")
    {
        $this->leavesColor = $leavesColor;
        $this->numberOfLeaves = $numberOfLeaves;
    }

    public function describeTree()
    {
        echo"Tento strom má $this->numberOfLeaves $this->leavesColor listů.";
}
}

$tree1=new Tree("žlutých", 1000);
$tree1->describeTree();
echo "<br/>";
$tree2=new Tree();
$tree2->leavesColor="zelených";
$tree2->numberOfLeaves=10000;
$tree2->describeTree();