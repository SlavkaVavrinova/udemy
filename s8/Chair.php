<?php

class Chair
{
public $type;
public $numberOfLegs;

    /**
     * @param $type
     * @param $numberOfLegs
     */
    public function __construct($type="", $numberOfLegs="")
    {
        $this->type = $type;
        $this->numberOfLegs = $numberOfLegs;
    }

    public function describeChair()
    {
        echo "Tato $this->type židle má $this->numberOfLegs nohy.";
    }

}

$chair1 = new Chair("jídelní",4);
$chair1->describeChair();

echo "<br/>";
$chair1 = new Chair();
$chair1->numberOfLegs=2;
$chair1->type="moderní";
$chair1->describeChair();