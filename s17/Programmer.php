<?php

abstract class Programmer
{
    public function solveTheProblem();

}

class EmployeeA extends Programmer{
    function solveTheProblem()
    {
        echo "Vyřešil jsem problém.";
    }
}

class EmployeeB extends Programmer{
    function solveTheProblem()
    {
        echo "Vyřešil jsem další problém.";
    }
}