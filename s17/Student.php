<?php

interface Student
{
    public function learnTheLesson();
}

class StudentA implements Student{
    function learnTheLesson()
    {
        echo "Já jsem se učila něco.";
    }
}

class StudentB implements Student{
    function learnTheLesson()
    {
        echo "Já jsem se učila něco2.";
    }
}

class StudentC implements Student{
    function learnTheLesson()
    {
        echo "Já jsem se učila něco3.";
    }
}