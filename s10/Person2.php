<?php

class Person2
{
    public $name;
    public $age;

    private function setInfo()
    {
        return "Jmenuji se " . $this->name . " a je mi " . $this->age . " let.";
    }

    public function getInfo()
    {
        echo $this->setInfo();
    }
}

$slavka = new Person2();
$slavka->name= "Slávka";
$slavka->age=36;
echo $slavka->getInfo();