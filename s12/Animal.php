<?php

class Animal
{
public $numberOfEyes ;

       public function __construct($numberOfEyes)
    {
        $this->numberOfEyes = $numberOfEyes;
    }

    public function showAnimalInfo()
    {
        echo "Nejvíce zvířat má $this->numberOfEyes oči.";
}
}

class Cat extends Animal {

    public function __construct($animal)
    {
        $this->numberOfEyes = $animal->numberOfEyes;
    }
}

$myanimal = new Animal(2);

$cat1 = new Cat($myanimal);
$cat1->showAnimalInfo();