<?php

class Person2
{
    public $name =  "Stanislava";
    public function showName ()
    {
echo "Jmenuji se $this->name.";
}
}

class Student extends Person2{

    public function showName()
    {
        echo "Jmenuji se $this->name, ale přátelé mi říkají Slávka.";
    }
}

$student1 = new Student();
$student1->showName();