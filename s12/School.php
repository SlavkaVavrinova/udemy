<?php

class School
{
    public $schoolName;
    public $schoolStep;


    public function __construct($schoolName, $schoolStep)
    {
        $this->schoolName = $schoolName;
        $this->schoolStep = $schoolStep;
    }

    public function showSchoolInfo()
    {
        echo "$this->schoolName <br/>";
        echo "$this->schoolStep";
    }
}


class Student extends School
{
    public $studentName;
    public $studentAge;


    public function __construct($studentName, $studentAge, $school)
    {
        $this->studentName = $studentName;
        $this->studentAge = $studentAge;

        $this->schoolName = $school->schoolName;
        $this->schoolStep = $school->schoolStep;
    }

    public function showStudentInfo()
    {
        echo "$this->studentName <br/>";
        echo "$this->studentAge <br/>";

        echo "$this->schoolName <br/>";
        echo "$this->schoolStep ";
    }
}

$mySchool = new School("VŠE", 4);
$student1 = new Student("Aleš", 15, $mySchool);
$student1->showStudentInfo();



