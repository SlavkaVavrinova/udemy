<?php

class Block
{
public $address;

       public function __construct($address)
    {
        $this->address = $address;
    }


    public function printAddress()
    {
        echo "Bydlím ve městě: ".$this->address;
}
}

class Appartment extends Block{

    public function __construct($block)
    {
        $this->address=$block->address;
    }
}

$myBlock = new Block("Nové Homole");

$appartment1 = new Appartment($myBlock);
$appartment1->printAddress();